Utilizes: Laravel, Vue, and Twitter Bootstrap

Enter the names of 2 actors and it will show you any movies in which they have both starred.

See it in action here:
http://moviesearch.bigcrazydog.com/