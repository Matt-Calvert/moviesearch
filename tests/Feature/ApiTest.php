<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ApiTest extends TestCase
{
    /** @test */
    public function can_retrieve_actors_in_same_movie()
    {
        $response = $this->post('/api/actors/search/same-movie/Edward Norton/Brad Pitt');

        $response->assertOk();

        $this->assertNotNull($response->json());
        $this->assertNull($response->json('not_found'));
    }

    /** @test */
    public function can_retrieve_primary_information_for_movie()
    { 
        $response = $this->post('/api/movie/info/550');

        $response->assertOk();

        $this->assertNotNull($response->json());
        $this->assertNull($response->json('not_found'));
    }

	/** @test */
	public function can_retrieve_movie_credits()
	{ 
		// get Fight Club
		$response = $this->post('/api/movie/credits/550');

		$response->assertOk();

        $this->assertNotNull($response->json());
        $this->assertNull($response->json('not_found'));
	}
}
