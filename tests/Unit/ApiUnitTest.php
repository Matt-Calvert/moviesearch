<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ApiUnitTest extends TestCase
{
    /** @test */
    public function api_information_exists_in_env_file()
    {
    	$key = env('MOVIE_API_KEY');
        $this->assertNotNull($key);
    }

    /** @test */
    public function api_url_exists_in_env_file() {
    	$url = env('MOVIE_API_URL');
        $this->assertNotNull($url);
    } 
}
