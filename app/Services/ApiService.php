<?php
namespace App\Services;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class ApiService {
	private $client;

	public function __construct() {
		$this->client = new Client;
	}

	public function sameMovie($actor1, $actor2) {
		$results = [];

		try {
			$actor1Id = $this->getActorId($actor1);
		} catch(Exception $e) {
			\Log::error('Problem retrieving actor information => ' . $actor1);
			\Log::error($e->getMessage());
		}

		try {
			$actor2Id = $this->getActorId($actor2);
		} catch(Exception $e) {
			\Log::error('Problem retrieving actor information => ' . $actor2);
			\Log::error($e->getMessage());
		}

		if($actor1Id === null || $actor2Id === null) {
			$results['results']['returned_a_null_value'] = [ 'actor_1' => $actor1Id, 'actor_2' => $actor2Id ];
		} else {
			$request = $this->client->get(env('MOVIE_API_URL'). "/3/discover/movie?with_people=$actor1Id,$actor2Id&sort_by=release_date.desc&api_key=" . env('MOVIE_API_KEY'));

			$results = $request->getBody()->getContents();
		}

		return $results;
	}

	public function movieInfo($id) {
		try {
			$request = $this->client->get(env('MOVIE_API_URL'). "/3/movie/$id?api_key=" . env('MOVIE_API_KEY'));
			$results = $request->getBody()->getContents();	
		} catch(Exception $e) {
			\Log::error('Problem retrieving movie information => ' . $id);
			\Log::error($e->getMessage());
		}

		return $results;
	}

	public function movieCredits($id) {
		try {
			$request = $this->client->get(env('MOVIE_API_URL'). "/3/movie/$id/credits?api_key=" . env('MOVIE_API_KEY'));
			$results = $request->getBody()->getContents();	
		} catch(Exception $e) {
			\Log::error('Problem retrieving movie credits => ' . $id);
			\Log::error($e->getMessage());
		}

		return $results;
	}

    private function getActorId($actor) {
    	try {
    		$url = env('MOVIE_API_URL'). '/3/search/person?api_key='. env('MOVIE_API_KEY') . '&language=en-US&query=' . urlencode(strtolower($actor)) . '&page=1&include_adult=false';
	    	$request = $this->client->get($url);
			$info = json_decode($request->getBody()->getContents());
		} catch(Exception $e) {
			\Log::error('Problem retrieving actor => ' . $actor);
			\Log::error($e->getMessage());
		}

		return (isset($info->results[0]) && $info->results[0]->id) ? $info->results[0]->id : null;	
	}
}