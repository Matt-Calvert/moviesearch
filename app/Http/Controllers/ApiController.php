<?php

namespace App\Http\Controllers;

use App\Services\ApiService;
use Illuminate\Http\Request;

class ApiController extends Controller
{
	public function __construct() {
		$this->api = new ApiService;
	}

    public function sameMovie($actor1, $actor2) {
    	return $this->api->sameMovie($actor1, $actor2);
    }

    public function movieInfo($id) {
    	return $this->api->movieInfo($id);
    }
    
    public function movieCredits($id) {
    	return $this->api->movieCredits($id);
    }
}
