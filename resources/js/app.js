import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import App from './components/App'
import Home from './components/Home'
import Movie from './components/pages/Movie'

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/movie/:id',
            name: 'movie',
            component: Movie
        },
        {
            path: '/:actor_1?/:actor_2?',
            name: 'home',
            component: Home
        }
    ],
})

const app = new Vue({
    el: '#app',
    components: { App },
    router,
})